import "./App.css";
import ReduxMini from "./redux/ReduxMini";

function App() {
  return (
    <div className="App">
      <ReduxMini />
    </div>
  );
}

export default App;
